package com.cima.apisicer.modelo;

import java.util.Date;

public class Analitico {
    private Integer tituloAraucano;
    private String  tituloNombre;
    private Integer responsableAcademica;
    private Integer propuesta;
    private String propuestaNombre;
    private String nroResolucionConeau;
    private Integer nroResolucionInstitucion;
    private Date fechaIngreso;
    private Date fechaEgreso;
    private String tituloAnteriorNivel;
    private String tituloAnteriorNacionalidad;
    private String tituloAnteriorInstitucion;
    private String tituloAnteriorDenominacion;
    private String tituloAnteriorRevalidado;
    private String tituloAnteriorNroResolucion;
    private String tituloAptoEjercicio;
    private String planVigente;
    private String actividadNombre;
    private String actividadCodigo;
    private Date fecha;
    private Double nota;
    private String resultado;
    private Integer folioFisico;
    private Integer actaResolucion;
    private Double promedio;
    private Double creditos;
    private Double promedioSinAplazos;
    private String formaAprobacion;
    private Integer planAlumno;
    private String  tieneSanciones;

    public Analitico(Integer tituloAraucano, String tituloNombre, Integer responsableAcademica, Integer propuesta, String propuestaNombre, String nroResolucionConeau, Integer nroResolucionInstitucion, Date fechaIngreso, Date fechaEgreso, String tituloAnteriorNivel, String tituloAnteriorNacionalidad, String tituloAnteriorInstitucion, String tituloAnteriorDenominacion, String tituloAnteriorRevalidado, String tituloAnteriorNroResolucion, String tituloAptoEjercicio, String planVigente, String actividadNombre, String actividadCodigo, Date fecha, Double nota, String resultado, Integer folioFisico, Integer actaResolucion, Double promedio, Double creditos, Double promedioSinAplazos, String formaAprobacion, Integer planAlumno, String tieneSanciones) {
        this.tituloAraucano = tituloAraucano;
        this.tituloNombre = tituloNombre;
        this.responsableAcademica = responsableAcademica;
        this.propuesta = propuesta;
        this.propuestaNombre = propuestaNombre;
        this.nroResolucionConeau = nroResolucionConeau;
        this.nroResolucionInstitucion = nroResolucionInstitucion;
        this.fechaIngreso = fechaIngreso;
        this.fechaEgreso = fechaEgreso;
        this.tituloAnteriorNivel = tituloAnteriorNivel;
        this.tituloAnteriorNacionalidad = tituloAnteriorNacionalidad;
        this.tituloAnteriorInstitucion = tituloAnteriorInstitucion;
        this.tituloAnteriorDenominacion = tituloAnteriorDenominacion;
        this.tituloAnteriorRevalidado = tituloAnteriorRevalidado;
        this.tituloAnteriorNroResolucion = tituloAnteriorNroResolucion;
        this.tituloAptoEjercicio = tituloAptoEjercicio;
        this.planVigente = planVigente;
        this.actividadNombre = actividadNombre;
        this.actividadCodigo = actividadCodigo;
        this.fecha = fecha;
        this.nota = nota;
        this.resultado = resultado;
        this.folioFisico = folioFisico;
        this.actaResolucion = actaResolucion;
        this.promedio = promedio;
        this.creditos = creditos;
        this.promedioSinAplazos = promedioSinAplazos;
        this.formaAprobacion = formaAprobacion;
        this.planAlumno = planAlumno;
        this.tieneSanciones = tieneSanciones;
    }

    public Integer getTituloAraucano() {
        return tituloAraucano;
    }

    public void setTituloAraucano(Integer tituloAraucano) {
        this.tituloAraucano = tituloAraucano;
    }

    public String getTituloNombre() {
        return tituloNombre;
    }

    public void setTituloNombre(String tituloNombre) {
        this.tituloNombre = tituloNombre;
    }

    public Integer getResponsableAcademica() {
        return responsableAcademica;
    }

    public void setResponsableAcademica(Integer responsableAcademica) {
        this.responsableAcademica = responsableAcademica;
    }

    public Integer getPropuesta() {
        return propuesta;
    }

    public void setPropuesta(Integer propuesta) {
        this.propuesta = propuesta;
    }

    public String getPropuestaNombre() {
        return propuestaNombre;
    }

    public void setPropuestaNombre(String propuestaNombre) {
        this.propuestaNombre = propuestaNombre;
    }

    public String getNroResolucionConeau() {
        return nroResolucionConeau;
    }

    public void setNroResolucionConeau(String nroResolucionConeau) {
        this.nroResolucionConeau = nroResolucionConeau;
    }

    public Integer getNroResolucionInstitucion() {
        return nroResolucionInstitucion;
    }

    public void setNroResolucionInstitucion(Integer nroResolucionInstitucion) {
        this.nroResolucionInstitucion = nroResolucionInstitucion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaEgreso() {
        return fechaEgreso;
    }

    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }

    public String getTituloAnteriorNivel() {
        return tituloAnteriorNivel;
    }

    public void setTituloAnteriorNivel(String tituloAnteriorNivel) {
        this.tituloAnteriorNivel = tituloAnteriorNivel;
    }

    public String getTituloAnteriorNacionalidad() {
        return tituloAnteriorNacionalidad;
    }

    public void setTituloAnteriorNacionalidad(String tituloAnteriorNacionalidad) {
        this.tituloAnteriorNacionalidad = tituloAnteriorNacionalidad;
    }

    public String getTituloAnteriorInstitucion() {
        return tituloAnteriorInstitucion;
    }

    public void setTituloAnteriorInstitucion(String tituloAnteriorInstitucion) {
        this.tituloAnteriorInstitucion = tituloAnteriorInstitucion;
    }

    public String getTituloAnteriorDenominacion() {
        return tituloAnteriorDenominacion;
    }

    public void setTituloAnteriorDenominacion(String tituloAnteriorDenominacion) {
        this.tituloAnteriorDenominacion = tituloAnteriorDenominacion;
    }

    public String getTituloAnteriorRevalidado() {
        return tituloAnteriorRevalidado;
    }

    public void setTituloAnteriorRevalidado(String tituloAnteriorRevalidado) {
        this.tituloAnteriorRevalidado = tituloAnteriorRevalidado;
    }

    public String getTituloAnteriorNroResolucion() {
        return tituloAnteriorNroResolucion;
    }

    public void setTituloAnteriorNroResolucion(String tituloAnteriorNroResolucion) {
        this.tituloAnteriorNroResolucion = tituloAnteriorNroResolucion;
    }

    public String getTituloAptoEjercicio() {
        return tituloAptoEjercicio;
    }

    public void setTituloAptoEjercicio(String tituloAptoEjercicio) {
        this.tituloAptoEjercicio = tituloAptoEjercicio;
    }

    public String getPlanVigente() {
        return planVigente;
    }

    public void setPlanVigente(String planVigente) {
        this.planVigente = planVigente;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }

    public String getActividadCodigo() {
        return actividadCodigo;
    }

    public void setActividadCodigo(String actividadCodigo) {
        this.actividadCodigo = actividadCodigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Integer getFolioFisico() {
        return folioFisico;
    }

    public void setFolioFisico(Integer folioFisico) {
        this.folioFisico = folioFisico;
    }

    public Integer getActaResolucion() {
        return actaResolucion;
    }

    public void setActaResolucion(Integer actaResolucion) {
        this.actaResolucion = actaResolucion;
    }

    public Double getPromedio() {
        return promedio;
    }

    public void setPromedio(Double promedio) {
        this.promedio = promedio;
    }

    public Double getCreditos() {
        return creditos;
    }

    public void setCreditos(Double creditos) {
        this.creditos = creditos;
    }

    public Double getPromedioSinAplazos() {
        return promedioSinAplazos;
    }

    public void setPromedioSinAplazos(Double promedioSinAplazos) {
        this.promedioSinAplazos = promedioSinAplazos;
    }

    public String getFormaAprobacion() {
        return formaAprobacion;
    }

    public void setFormaAprobacion(String formaAprobacion) {
        this.formaAprobacion = formaAprobacion;
    }

    public Integer getPlanAlumno() {
        return planAlumno;
    }

    public void setPlanAlumno(Integer planAlumno) {
        this.planAlumno = planAlumno;
    }

    public String getTieneSanciones() {
        return tieneSanciones;
    }

    public void setTieneSanciones(String tieneSanciones) {
        this.tieneSanciones = tieneSanciones;
    }
}
