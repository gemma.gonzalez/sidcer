package com.cima.apisicer.excepcion;

public class MensajeError extends RuntimeException{
    public MensajeError(String message) {
        super(message);
    }
}
