package com.cima.apisicer.excepcion;

import com.cima.apisicer.respuestaExcep.StaRespuestaCont;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;

@ControllerAdvice
public class ExcepcionHandler {

    @ExceptionHandler(MensajeError.class)
    public ResponseEntity<Object> handleConflict(MensajeError ex, ServletWebRequest request) {

        return  new ResponseEntity<Object>(
                new StaRespuestaCont()
                        .message(ex.getMessage())
                        .details(request.getRequest().getRequestURI())
                        .success(false)
                        .build()
                ,new HttpHeaders(), HttpStatus.FORBIDDEN);
    }
}
