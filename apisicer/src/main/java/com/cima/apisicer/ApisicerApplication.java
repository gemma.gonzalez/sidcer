package com.cima.apisicer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApisicerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApisicerApplication.class, args);
	}

}

