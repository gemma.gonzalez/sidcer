package com.cima.apisicer.respuestaExcep;

import java.util.Date;

public class StaRespuestaCont {

    protected Date timestamp;
    protected String message;
    protected String details;
    protected Boolean success;
    protected Object result;

    public StaRespuestaCont timestamp(Date timestamp) {
        this.timestamp = timestamp;
        return  this;
    }
    public StaRespuestaCont message(String message) {
        this.message = message;
        return  this;
    }
    public StaRespuestaCont details(String details) {
        this.details = details;
        return this;
    }

    public StaRespuestaCont success(Boolean success) {
        this.success = success;
        return this;
    }

    public StaRespuesta build() {
        this.timestamp = new Date();
        return new StaRespuesta(this);
    }


    public StaRespuestaCont result(Object result) {
        this.result = result;
        return this;
    }
}
