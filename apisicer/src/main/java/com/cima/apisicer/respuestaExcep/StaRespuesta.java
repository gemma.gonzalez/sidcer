package com.cima.apisicer.respuestaExcep;

import java.util.Date;
import java.util.Objects;

public class StaRespuesta {

    private Object result;
    private Date timestamp;
    private String message;
    private String details;
    private Boolean success;

    protected StaRespuesta(StaRespuestaCont staRespuestaCont) {
        Objects.requireNonNull(staRespuestaCont);
        this.timestamp = staRespuestaCont.timestamp;
        this.message = staRespuestaCont.message;
        this.details = staRespuestaCont.details;
        this.success = staRespuestaCont.success;
        this.result = staRespuestaCont.result;
    }

    public StaRespuesta() {

    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
