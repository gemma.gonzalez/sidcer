package com.cima.apisicer.service;

import com.cima.apisicer.controlador.consume.FiltroConsume;
import com.cima.apisicer.controlador.consume.TituloConsume;
import com.cima.apisicer.controlador.respuesta.CodigoRespuesta;
import com.cima.apisicer.controlador.respuesta.PersonaRespuesta;
import com.cima.apisicer.modelo.Codigo;
import com.cima.apisicer.modelo.Persona;

import java.util.List;


public interface ServicioPersona {

   CodigoRespuesta traerCodigoPersona(FiltroConsume filter);
   Boolean getId(Long id);
   List<?> traerDatosDeTitulos(TituloConsume filtro);
   PersonaRespuesta TraerDatosPersona(Long id);
  // Persona TraerDatosPersona(Long id);
}
