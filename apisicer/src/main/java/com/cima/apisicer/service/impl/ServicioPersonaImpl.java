package com.cima.apisicer.service.impl;

import com.cima.apisicer.controlador.consume.FiltroConsume;
import com.cima.apisicer.controlador.consume.TituloConsume;
import com.cima.apisicer.controlador.respuesta.AnaliticoRespuesta;
import com.cima.apisicer.controlador.respuesta.CodigoRespuesta;
import com.cima.apisicer.controlador.respuesta.PersonaRespuesta;
import com.cima.apisicer.modelo.Codigo;
import com.cima.apisicer.modelo.Persona;
import com.cima.apisicer.service.ServicioPersona;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ServicioPersonaImpl implements ServicioPersona {

    private static List<Persona> personas;
    private static List<PersonaRespuesta> personas1;
    private static List<AnaliticoRespuesta> datosAnaliticos;
    private  static Codigo obtenerCod;
    private Class <PersonaRespuesta> PersonaRespuesta;


    @Override
    public CodigoRespuesta traerCodigoPersona(FiltroConsume filter) {
        CodigoRespuesta obtenerCod= new CodigoRespuesta();
        if((filter.getNumeroDocumento()== 34187260)&&(filter.getTipoDocumento()==0)&&(filter.getPais()==54)){
            obtenerCod.setCodigo(2813);
            return obtenerCod;
        }
        else {
            return null;
        }
    }
    @Override
    public Boolean getId(Long id) {
        if(id==2813){
            return true;
        }
        else
        {
            return  false;
        }
    }

    @Override
    public List<?> traerDatosDeTitulos(TituloConsume filtro) {
        List<AnaliticoRespuesta> datosAnaliticos = new ArrayList<>();
        Date fechaIngreso= null,fechaEgreso= null,fecha= null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fechaIngreso = sdf.parse("2007-02-09");
            fecha = sdf.parse("2007-07-10");
            fechaEgreso = sdf.parse("2017-10-06");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        datosAnaliticos.add(new AnaliticoRespuesta(92,
                "Ingeniera Agronoma",
                128,
                1,
                "INGENIERIA AGRONOMICA",
                "",
                null,
                fechaIngreso,
                fechaEgreso,
                "Secundario",
                "Argentino",
                "COLEGIO POLIMODAL NRO. 5 GDOR. FRANCISCO RAMON GALINDEZ",
                "MODALIDAD ECONOMÍA Y GESTIN DE LAS ORGANIZACIONES",
                "",
                "",
                "SI",
                "SI",
                "Matemática I",
                "1C1",
                fecha,
                6.00,
                "Aprobado",
                119,
                001007760,
                6.165,
                0.00,
                7.078,
                "E",
                2015,
                "N"));
        //analitico.add(new Analitico(92,"Ingeniera Agronoma",128,1,"INGENIERIA AGRONOMICA",,null,fechaIngreso,fechaEgreso,"Secundario","Argentino","COLEGIO POLIMODAL NRO. 5 GDOR. FRANCISCO RAMON GALINDEZ","MODALIDAD ECONOMÍA Y GESTI\u00d3N DE LAS ORGANIZACIONES","", "","SI","SI","Seminario de Campo","1C40",fecha,6,"Aprobado",119,001007760,6.165,0.00,7.078,"E",2015,"N"));
        return datosAnaliticos;
    }
    @Override
    public PersonaRespuesta TraerDatosPersona(Long id) {
        List<PersonaRespuesta> pers = new ArrayList<PersonaRespuesta>();
        Date fecha= null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fecha = sdf.parse("1989-02-21");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        pers.add(new PersonaRespuesta("REARTES","NOELIA DE LOS ANGELES","Femenino", fecha,"Argentino","",54,54,"Argentina",5410,"Catamarca",10029,"San Fernando del Valle de Catamarca","Calle 123"));
        for (PersonaRespuesta pr: pers){
            if (id==2813){
                return pr;
            }
        }
        return  null;
    }
}
