package com.cima.apisicer.controlador;

import com.cima.apisicer.controlador.consume.FiltroConsume;

import com.cima.apisicer.controlador.consume.TituloConsume;
import com.cima.apisicer.controlador.respuesta.CodigoRespuesta;
import com.cima.apisicer.controlador.respuesta.PersonaRespuesta;
import com.cima.apisicer.excepcion.MensajeError;
import com.cima.apisicer.service.ServicioPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/apisicer/rest/personas")
public class PersonaControladorRest {

    @Autowired
    private ServicioPersona servicioPersona;

    @RequestMapping(value = {""}, method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ResponseEntity<?> traerPersona(
            @RequestParam(value = "pais", required = true) Integer  pais,
            @RequestParam(value = "tipo_documento", required = true) Integer tipoDocumento,
            @RequestParam(value = "numero_documento", required = true) Integer numeroDocumento){

        FiltroConsume filter = new FiltroConsume();
        filter.setPais(pais);
        filter.setTipoDocumento(tipoDocumento);
        filter.setNumeroDocumento(numeroDocumento);
        CodigoRespuesta BuscaCodigo = servicioPersona.traerCodigoPersona(filter);
        if (BuscaCodigo == null) {
           // return new ResponseEntity(HttpStatus.NO_CONTENT);
            //return new ResponseEntity(new MensajeError("Dato no encontrado"), HttpStatus.NO_CONTENT);
            throw new MensajeError("Datos no encontrados");
        }
        return new ResponseEntity(BuscaCodigo, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}/datospersonales"}, method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ResponseEntity<?> traerDatosPersona(@PathVariable("id") Long id) {

        Boolean datos = servicioPersona.getId(id);
        if(datos==true){
            PersonaRespuesta Respuesta = servicioPersona.TraerDatosPersona(id);
            return new ResponseEntity(Respuesta, HttpStatus.OK);
        }
        else {
            throw new MensajeError("No existe ID");
            //return new ResponseEntity(new MensajeError("Dato no encontrado"+id), HttpStatus.NO_CONTENT);

        }
    }
    @RequestMapping(value = {"/{id}/datosanaliticos"}, method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ResponseEntity<List<?>> DatosAnaliticos(@PathVariable("id") Integer codigo,
                                                                 @RequestParam(value = "codigoTituloAraucano", required = true) Integer tituloAraucano) {
        //sino recibo el codigo del titulo entonces entonces le coloco cero y tengo q mostrar todos los titulos q el alumno haya obtenido
        TituloConsume filtro = new TituloConsume();
        filtro.setCodigo(codigo);
        if (tituloAraucano ==null) {
            filtro.setTituloAraucano(0);
        }
        else {
                filtro.setTituloAraucano(tituloAraucano);
        }
        List<?> BuscatDatos= servicioPersona.traerDatosDeTitulos(filtro);
        if (BuscatDatos != null) {
            return new ResponseEntity(BuscatDatos, HttpStatus.OK);
        }
       // return new ResponseEntity(HttpStatus.NO_CONTENT);
        throw new MensajeError("No existe ID");
    }
}
