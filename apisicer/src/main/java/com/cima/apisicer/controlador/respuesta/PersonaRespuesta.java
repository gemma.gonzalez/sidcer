package com.cima.apisicer.controlador.respuesta;
import com.fasterxml.jackson.annotation.JsonGetter;
import java.util.Date;

public class PersonaRespuesta {

    private String apellido;
    private String nombre;
    private String genero;
    private Date fechaNacimiento;
    private String nacionalidad;
    private String legajo;
    private Integer paisOrigen;
    private Integer codigoPaisprocedencia;
    private String nombrePaisProcedencia;
    private Integer codigoProvinciaProcedencia;
    private String nombreProvinciaProcedencia;
    private Integer codigoLocalidadProcedencia;
    private String nombreLocalidadProcedencia;
    private  String direccionProcedencia;

    public PersonaRespuesta(String apellido, String nombre, String genero, Date fechaNacimiento, String nacionalidad, String legajo, Integer paisOrigen, Integer codigoPaisprocedencia, String nombrePaisProcedencia, Integer codigoProvinciaProcedencia, String nombreProvinciaProcedencia, Integer codigoLocalidadProcedencia, String nombreLocalidadProcedencia, String direccionProcedencia) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.nacionalidad = nacionalidad;
        this.legajo = legajo;
        this.paisOrigen = paisOrigen;
        this.codigoPaisprocedencia = codigoPaisprocedencia;
        this.nombrePaisProcedencia = nombrePaisProcedencia;
        this.codigoProvinciaProcedencia = codigoProvinciaProcedencia;
        this.nombreProvinciaProcedencia = nombreProvinciaProcedencia;
        this.codigoLocalidadProcedencia = codigoLocalidadProcedencia;
        this.nombreLocalidadProcedencia = nombreLocalidadProcedencia;
        this.direccionProcedencia = direccionProcedencia;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @JsonGetter("fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }
    @JsonGetter("pais_origen")
    public Integer getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(Integer paisOrigen) {
        this.paisOrigen = paisOrigen;
    }
    @JsonGetter("codigo_pais_procedencia")
    public Integer getCodigoPaisprocedencia() {
        return codigoPaisprocedencia;
    }

    public void setCodigoPaisprocedencia(Integer codigoPaisprocedencia) {
        this.codigoPaisprocedencia = codigoPaisprocedencia;
    }

    @JsonGetter("nombre_pais_procedencia")
    public String getNombrePaisProcedencia() {
        return nombrePaisProcedencia;
    }

    public void setNombrePaisProcedencia(String nombrePaisProcedencia) {
        this.nombrePaisProcedencia = nombrePaisProcedencia;
    }
    @JsonGetter("codigo_provincia_procedencia")
    public Integer getCodigoProvinciaProcedencia() {
        return codigoProvinciaProcedencia;
    }

    public void setCodigoProvinciaProcedencia(Integer codigoProvinciaProcedencia) {
        this.codigoProvinciaProcedencia = codigoProvinciaProcedencia;
    }
    @JsonGetter("nombre_provincia_procedencia")
    public String getNombreProvinciaProcedencia() {
        return nombreProvinciaProcedencia;
    }

    public void setNombreProvinciaProcedencia(String nombreProvinciaProcedencia) {
        this.nombreProvinciaProcedencia = nombreProvinciaProcedencia;
    }
    @JsonGetter("codigo_localidad_procedencia")
    public Integer getCodigoLocalidadProcedencia() {
        return codigoLocalidadProcedencia;
    }

    public void setCodigoLocalidadProcedencia(Integer codigoLocalidadProcedencia) {
        this.codigoLocalidadProcedencia = codigoLocalidadProcedencia;
    }
    @JsonGetter("nombre_localidad_procedencia")
    public String getNombreLocalidadProcedencia() {
        return nombreLocalidadProcedencia;
    }

    public void setNombreLocalidadProcedencia(String nombreLocalidadProcedencia) {
        this.nombreLocalidadProcedencia = nombreLocalidadProcedencia;
    }
    @JsonGetter("direccion_procedencia")
    public String getDireccionProcedencia() {
        return direccionProcedencia;
    }

    public void setDireccionProcedencia(String direccionProcedencia) {
        this.direccionProcedencia = direccionProcedencia;
    }

}
