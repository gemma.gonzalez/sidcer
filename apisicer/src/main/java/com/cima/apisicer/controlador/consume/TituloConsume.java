package com.cima.apisicer.controlador.consume;

public class TituloConsume {

    private Integer codigo;
    private Integer TituloAraucano;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getTituloAraucano() {
        return TituloAraucano;
    }

    public void setTituloAraucano(Integer tituloAraucano) {
        TituloAraucano = tituloAraucano;
    }
}
