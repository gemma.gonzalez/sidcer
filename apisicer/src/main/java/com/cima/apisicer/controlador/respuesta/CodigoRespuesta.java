package com.cima.apisicer.controlador.respuesta;

public class CodigoRespuesta {

    private Integer codigo;

    public Integer getCodigo() {
        return codigo;
    }
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
