package com.cima.apisicer.controlador.consume;

public class FiltroConsume {

    private Integer pais;
    private Integer tipoDocumento;
    private Integer numeroDocumento;

    public Integer getPais() {
        return pais;
    }
    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Integer numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
}
