package com.cima.apisicer.controlador.respuesta;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.Date;

public class AnaliticoRespuesta {

    private Integer tituloAraucano;
    private String  tituloNombre;
    private Integer responsableAcademica;
    private Integer propuesta;
    private String propuestaNombre;
    private String nroResolucionConeau;
    private Integer nroResolucionInstitucion;
    private Date fechaIngreso;
    private Date fechaEgreso;
    private String tituloAnteriorNivel;
    private String tituloAnteriorNacionalidad;
    private String tituloAnteriorInstitucion;
    private String tituloAnteriorDenominacion;
    private String tituloAnteriorRevalidado;
    private String tituloAnteriorNroResolucion;
    private String tituloAptoEjercicio;
    private String planVigente;
    private String actividadNombre;
    private String actividadCodigo;
    private Date fecha;
    private Double nota;
    private String resultado;
    private Integer folioFisico;
    private Integer actaResolucion;
    private Double promedio;
    private Double creditos;
    private Double promedioSinAplazos;
    private String formaAprobacion;
    private Integer planAlumno;
    private String  tieneSanciones;

    public AnaliticoRespuesta(Integer tituloAraucano, String tituloNombre, Integer responsableAcademica, Integer propuesta, String propuestaNombre, String nroResolucionConeau, Integer nroResolucionInstitucion, Date fechaIngreso, Date fechaEgreso, String tituloAnteriorNivel, String tituloAnteriorNacionalidad, String tituloAnteriorInstitucion, String tituloAnteriorDenominacion, String tituloAnteriorRevalidado, String tituloAnteriorNroResolucion, String tituloAptoEjercicio, String planVigente, String actividadNombre, String actividadCodigo, Date fecha, Double nota, String resultado, Integer folioFisico, Integer actaResolucion, Double promedio, Double creditos, Double promedioSinAplazos, String formaAprobacion, Integer planAlumno, String tieneSanciones) {
        this.tituloAraucano = tituloAraucano;
        this.tituloNombre = tituloNombre;
        this.responsableAcademica = responsableAcademica;
        this.propuesta = propuesta;
        this.propuestaNombre = propuestaNombre;
        this.nroResolucionConeau = nroResolucionConeau;
        this.nroResolucionInstitucion = nroResolucionInstitucion;
        this.fechaIngreso = fechaIngreso;
        this.fechaEgreso = fechaEgreso;
        this.tituloAnteriorNivel = tituloAnteriorNivel;
        this.tituloAnteriorNacionalidad = tituloAnteriorNacionalidad;
        this.tituloAnteriorInstitucion = tituloAnteriorInstitucion;
        this.tituloAnteriorDenominacion = tituloAnteriorDenominacion;
        this.tituloAnteriorRevalidado = tituloAnteriorRevalidado;
        this.tituloAnteriorNroResolucion = tituloAnteriorNroResolucion;
        this.tituloAptoEjercicio = tituloAptoEjercicio;
        this.planVigente = planVigente;
        this.actividadNombre = actividadNombre;
        this.actividadCodigo = actividadCodigo;
        this.fecha = fecha;
        this.nota = nota;
        this.resultado = resultado;
        this.folioFisico = folioFisico;
        this.actaResolucion = actaResolucion;
        this.promedio = promedio;
        this.creditos = creditos;
        this.promedioSinAplazos = promedioSinAplazos;
        this.formaAprobacion = formaAprobacion;
        this.planAlumno = planAlumno;
        this.tieneSanciones = tieneSanciones;
    }

    @JsonGetter("titulo_araucano")
    public Integer getTituloAraucano() {
        return tituloAraucano;
    }

    public void setTituloAraucano(Integer tituloAraucano) {
        this.tituloAraucano = tituloAraucano;
    }
    @JsonGetter("titulo_nombre")
    public String getTituloNombre() {
        return tituloNombre;
    }

    public void setTituloNombre(String tituloNombre) {
        this.tituloNombre = tituloNombre;
    }
    @JsonGetter("responsable_academica")
    public Integer getResponsableAcademica() {
        return responsableAcademica;
    }

    public void setResponsableAcademica(Integer responsableAcademica) {
        this.responsableAcademica = responsableAcademica;
    }

    public Integer getPropuesta() {
        return propuesta;
    }

    public void setPropuesta(Integer propuesta) {
        this.propuesta = propuesta;
    }
    @JsonGetter("propuesta_nombre")
    public String getPropuestaNombre() {
        return propuestaNombre;
    }

    public void setPropuestaNombre(String propuestaNombre) {
        this.propuestaNombre = propuestaNombre;
    }
    @JsonGetter("nro_resolucion_coneau")
    public String getNroResolucionConeau() {
        return nroResolucionConeau;
    }

    public void setNroResolucionConeau(String nroResolucionConeau) {
        this.nroResolucionConeau = nroResolucionConeau;
    }
    @JsonGetter("nro_resolución_institucion")
    public Integer getNroResolucionInstitucion() {
        return nroResolucionInstitucion;
    }

    public void setNroResolucionInstitucion(Integer nroResolucionInstitucion) {
        this.nroResolucionInstitucion = nroResolucionInstitucion;
    }
    @JsonGetter("fecha_ingreso")
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    @JsonGetter("fecha_egreso")
    public Date getFechaEgreso() {
        return fechaEgreso;
    }

    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }
    @JsonGetter("titulo_anterior_nivel")
    public String getTituloAnteriorNivel() {
        return tituloAnteriorNivel;
    }

    public void setTituloAnteriorNivel(String tituloAnteriorNivel) {
        this.tituloAnteriorNivel = tituloAnteriorNivel;
    }
    @JsonGetter("titulo_anterior_nacionalidad")
    public String getTituloAnteriorNacionalidad() {
        return tituloAnteriorNacionalidad;
    }

    public void setTituloAnteriorNacionalidad(String tituloAnteriorNacionalidad) {
        this.tituloAnteriorNacionalidad = tituloAnteriorNacionalidad;
    }
    @JsonGetter("titulo_anterior_institucion")
    public String getTituloAnteriorInstitucion() {
        return tituloAnteriorInstitucion;
    }

    public void setTituloAnteriorInstitucion(String tituloAnteriorInstitucion) {
        this.tituloAnteriorInstitucion = tituloAnteriorInstitucion;
    }
    @JsonGetter("titulo_anterior_denominacion")
    public String getTituloAnteriorDenominacion() {
        return tituloAnteriorDenominacion;
    }

    public void setTituloAnteriorDenominacion(String tituloAnteriorDenominacion) {
        this.tituloAnteriorDenominacion = tituloAnteriorDenominacion;
    }
    @JsonGetter("titulo_anterior_revalidado")
    public String getTituloAnteriorRevalidado() {
        return tituloAnteriorRevalidado;
    }

    public void setTituloAnteriorRevalidado(String tituloAnteriorRevalidado) {
        this.tituloAnteriorRevalidado = tituloAnteriorRevalidado;
    }
    @JsonGetter("titulo_anterior_nro_resolucion")
    public String getTituloAnteriorNroResolucion() {
        return tituloAnteriorNroResolucion;
    }

    public void setTituloAnteriorNroResolucion(String tituloAnteriorNroResolucion) {
        this.tituloAnteriorNroResolucion = tituloAnteriorNroResolucion;
    }
    @JsonGetter("titulo_apto_ejercicio")
    public String getTituloAptoEjercicio() {
        return tituloAptoEjercicio;
    }

    public void setTituloAptoEjercicio(String tituloAptoEjercicio) {
        this.tituloAptoEjercicio = tituloAptoEjercicio;
    }
    @JsonGetter("plan_vigente")
    public String getPlanVigente() {
        return planVigente;
    }

    public void setPlanVigente(String planVigente) {
        this.planVigente = planVigente;
    }
    @JsonGetter("actividad_nombre")
    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }
    @JsonGetter("actividad_codigo")
    public String getActividadCodigo() {
        return actividadCodigo;
    }

    public void setActividadCodigo(String actividadCodigo) {
        this.actividadCodigo = actividadCodigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    @JsonGetter("folio_fisico")
    public Integer getFolioFisico() {
        return folioFisico;
    }

    public void setFolioFisico(Integer folioFisico) {
        this.folioFisico = folioFisico;
    }
    @JsonGetter("acta_resolucion")
    public Integer getActaResolucion() {
        return actaResolucion;
    }

    public void setActaResolucion(Integer actaResolucion) {
        this.actaResolucion = actaResolucion;
    }

    public Double getPromedio() {
        return promedio;
    }

    public void setPromedio(Double promedio) {
        this.promedio = promedio;
    }

    public Double getCreditos() {
        return creditos;
    }

    public void setCreditos(Double creditos) {
        this.creditos = creditos;
    }
    @JsonGetter("promedio_sin_aplazos")
    public Double getPromedioSinAplazos() {
        return promedioSinAplazos;
    }

    public void setPromedioSinAplazos(Double promedioSinAplazos) {
        this.promedioSinAplazos = promedioSinAplazos;
    }
    @JsonGetter("forma_aprobacion")
    public String getFormaAprobacion() {
        return formaAprobacion;
    }

    public void setFormaAprobacion(String formaAprobacion) {
        this.formaAprobacion = formaAprobacion;
    }
    @JsonGetter("plan_alumno")
    public Integer getPlanAlumno() {
        return planAlumno;
    }

    public void setPlanAlumno(Integer planAlumno) {
        this.planAlumno = planAlumno;
    }
    @JsonGetter("tiene_sanciones")
    public String getTieneSanciones() {
        return tieneSanciones;
    }

    public void setTieneSanciones(String tieneSanciones) {
        this.tieneSanciones = tieneSanciones;
    }
}
